// ==UserScript==
// @name         Get info about testFile and Chain
// @version      1.5.1
// @description  try to take over the world!
// @author       Volodymyr N
// @include      /^http:\/\/jenkins.*\/job\/.*\/[\d]+\/.*\/.*/*/
// @icon         https://www.google.com/s2/favicons?domain=temerix.com
// @grant        none
// @updateURL    https://bitbucket.org/VolodymyrNik/plugins-and-helpers/raw/master/Get%20info%20about%20testFile%20and%20Chain.user.js
// ==/UserScript==

function getAllTime() {
    let data = Math.round(Array.prototype.slice.call(
        document.querySelectorAll(".node__leaf .node__stats")).map(data => Number.parseInt(data.innerText.toString().replaceAll("s ", "").replaceAll("ms", ""))).reduce((a, b) => a + b, 0) / 100) / 10 + "s";
    document.querySelector("#time").innerText = " " + data
}

setTimeout(main, 1500);

async function main() {
    let title = document.querySelectorAll(".pane__title-text");

    if (title.length > 0) {
        title[0].innerHTML = "<span style='font-size:10pt' id='test_file' class=''></span>";
        document.querySelector("#test_file").innerHTML = " " + await getData();
        document.querySelector(".pane__search").style["width"] = "50%";

        //time
        document.querySelector(".marks-toggle__items").innerHTML += "<div class=\"marks-toggle__item\">\n" +
            "<span class=\"n-label-mark n-label_mark_newFailed\" data-tooltip=\"Show New Failed test results\"><span class=\"fa fa-clock-o [object Object]\" data-tooltip=\"time.\"></span></span>\n" +
            "</div><span id='time'></span>";

        document.querySelectorAll(".fa-clock-o")[1].addEventListener("mouseup", function () {
            getAllTime();
        });
    } else {
        setTimeout(main, 1500);
    }

}

async function getData() {
    const regex = /.*\/\d+\//gm;
    const regexForFile = new RegExp("-Dtestfile=([\\w.\\/]+)", 's');
    const regexForChain = new RegExp("-DCHAIN=([\\w.\\/]+)", 's');
    const regexForServer = new RegExp("origin\/([\\d.]+)", 's');
    const regexCustomServer = new RegExp("origin/((?:SUPQA|QA_for_SUP)-[\\d]+)", 's');
    const regexForChangeRcToEmulate = new RegExp("(\\?|RC-STAGE|STAGE-RC)-", 's');

    let urlBase = document.location.href;
    let urlRegex = regex.exec(urlBase);
    let urlConsole = urlRegex + "consoleText";
    let urlJob = urlRegex.toString().replace(new RegExp('/\\d+\/', ''), "");
    let urlConvert = urlJob.replace(regexForChangeRcToEmulate, "EMULATE-");
    let urlBuild = urlConvert + "\/build?delay=0sec";
    try {
        let response = await fetch(urlConsole);
        if (response.ok) { // если HTTP-статус в диапазоне 200-299
            // получаем тело ответа (см. про этот метод ниже)
            let text = await response.text();
            return "<a href='http://jenkins.fornova.temerix.com/view/Emulate/'>" +(regexForFile.test(text)?regexForFile.exec(text)[1]:"allTests") + "<\/a> " +
                "[<a href='" + urlJob + "'>J<\/a>] " +
                "[<a href='" + urlBuild + "'>B<\/a>] " +
                (urlJob.match(regexForChangeRcToEmulate) ? "[<a href='" + urlConvert + "'>E<\/a>] " : "[<a href='" + urlRegex + "retry'>R<\/a>] ") + "| " + regexForChain.exec(text)[1] + " " +
                "| " + (text.match(regexForServer) ? regexForServer.exec(text)[1] : text.match(regexCustomServer) ? regexCustomServer.exec(text)[1] : "");
        } else {
            alert("Error HTTP: " + response.status);
        }
    } catch {
    }
}



