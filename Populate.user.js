// ==UserScript==
// @name         Populate
// @version      2.0
// @description  Take information about all servers version
// @author       Volodymyr N
// @match        *.fornova-admin.temerix.com/reports?t=*
// @icon         https://www.google.com/s2/favicons?domain=temerix.com
// @grant        none
// @updateURL https://bitbucket.org/VolodymyrNik/plugins-and-helpers/raw/master/Populate.user.js
// ==/UserScript==

(function () {
    'use strict';

    let chainsName = ["Dusit", "Kerzner", "Dorint", "Compass", "Accor", "Accor Turkey", "The Ascott", "Emaar", "Danubius Hotels", "Rixos"];
    let chainsData = [];
    let todayLong;
    let startLong;
    let token = localStorage.token;

    const url = new URL(document.URL);
    const searchParams = new URLSearchParams(url.search).get('t');
    const server = url.host.split(".")[0];
    main();

    async function main() {
        await sleep(5000);
        if (searchParams != null && confirm("Run populate for: " + chainsName)) {
            console.log(searchParams + " days");
            let today = new Date();
            today.setHours(0, 0, 0, 0);
            let timezoneOffset = today.getTimezoneOffset(); // получаем разницу в минутах между местным временем и UTC
            todayLong = today.getTime() - timezoneOffset * 60 * 1000; // добавляем разницу в миллисекундах к дате
            todayLong = todayLong - (24 * 60 * 60 * 1000);
            startLong = todayLong - ((searchParams - 1) * 24 * 60 * 60 * 1000);

            getChainsData();
            console.log(chainsData);
            (async function () {
                for (let chain of chainsData) {
                    const isSuccessful = await populate(chain);
                    console.log(isSuccessful ? "YES" : "NOT");
                    await sleep(1000);
                }
                console.log("Finished!");
            })();
        } else {
            console.log("Not days params");
        }
    }

    async function populate(chain) {
        const url = "http://" + server + ".fornova.temerix.com/" +
            "api/v1/ri/reports/populate-cache/" + chain[1] + "?isAutomaticRCA=false&from=" + startLong + "&to=" + todayLong;

        console.log("try " + chain[0]);
        try {
            const response = await fetch(url, {
                headers: {
                    'token': token
                }, timeout: 120000
            });
            const data = await response.json();
            console.log(chain[0] + " " + JSON.stringify(data))
            const isSuccessful = data.status != "error";

            let row = document.evaluate("//*[contains(@id, '" + chain[1] + "')]", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0)
            row.bgColor = isSuccessful ? "#74f797" : "#f77474";
            await waitGenerate(chain[1]);
            return isSuccessful;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    async function waitGenerate(chain) {
        const url = "http://" + server + ".fornova.temerix.com/api/v1/ri/";
        let isGenerating = true;
        for (let i = 0; i < 24 && isGenerating; i++) {
            const response = await fetch(url, {
                headers: {
                    'token': token
                }
            });
            const data = await response.json();
            isGenerating = data.data.items.filter(e => e._id == chain)[0].systemStatus == "generating";
            console.log("generating");
            await sleep(10000);
        }
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    function getChainsData() {
        let elements = document.querySelectorAll(".ui-row-ltr");
        for (let element of elements) {
            let chainName = element.querySelectorAll("td")[3].textContent;
            if (chainsName.includes(chainName)) {
                let chainid = element.querySelectorAll("td")[1].textContent;
                chainsData.push([chainName, chainid, element]);
            }
        }
    }
})();