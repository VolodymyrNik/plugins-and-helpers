// ==UserScript==
// @name         Servers version
// @version      1.2
// @description  Take information about all servers version
// @author       Volodymyr N
// @match        http://jenkins.fornova.temerix.com/*
// @icon         https://www.google.com/s2/favicons?domain=temerix.com
// @grant        none
// @updateURL https://bitbucket.org/VolodymyrNik/plugins-and-helpers/raw/master/Servers.user.js
// ==/UserScript==
(function () {
    'use strict';
    let parentElement = document.querySelectorAll("#tasks");
    if (parentElement.length > 0) {
        parentElement[0].appendChild(
            createElementFromHTML("" +
                "<div class='task'><span class='task-link-wrapper'><a class='myUrlServer task-link' class='task-icon-link'>" +
                "<span class='task-icon-link'>" +
                "<img style='transform: rotate(33deg);' src='/static/1dab921a/images/24x24/computer.png' " +
                "style='width: 24px; height: 24px; width: 24px; height: 24px; margin: 2px;' class='icon-next icon-md'>" +
                "</span>" +
                "<span class='task-link-text myUrlServer'>Servers</span>" +
                "</a></span></div>"));

        let notificationBtns = document.querySelectorAll(".myUrlServer")
        for (let i = 0; i < notificationBtns.length; i++) {
            notificationBtns[i].addEventListener("mouseup", function () {
                getStatus();
            });
        }
    }

    async function getStatus() {
        let servers = ['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10', 'q11', 'q12', 'q13', 'rc', 'staging'];
        let allStatus = "";
        for (let i = 0; i < servers.length; i++) {
            fetch("http://" + servers[i] + ".fornova.temerix.com/api/v1/version")
                .then(response => response.json())
                .then(data => allStatus += "<p>" + servers[i] + " " + data.data.supernova + " | " + data.data.supernova_TAG) + "\r\n</p>";
            await sleep(60);
        }
        allStatus += "<\hr>";
        fetch("http://rc.api.supernova-travel.com/api/v1/version")
            .then(response => response.json())
            .then(data => allStatus += "<p>RC Israel " + data.data.supernova) + "\r\n</p>";
        fetch("http://staging.api.supernova-travel.com/api/v1/version")
            .then(response => response.json())
            .then(data => allStatus += "<p>Staging Israel " + data.data.supernova) + "\r\n</p>";
        fetch("https://apidms.fornova.com/api/v1/version")
            .then(response => response.json())
            .then(data => allStatus += "<p><b>Master " + data.data.supernova) + "</b>\r\n</p>";
        await sleep(800);

        let countLine = allStatus.split("<p>").length;
        let newWin = popupCenter("", "", 150, countLine * 25);
        newWin.document.write(allStatus);
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    function popupCenter(url, title, w, h) {
        // Fixes dual-screen position                             Most browsers      Firefox
        const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
        const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

        const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        const systemZoom = width / window.screen.availWidth;
        const left = (width - w) / 2 / systemZoom + dualScreenLeft
        const top = (height - h) / 2 / systemZoom + dualScreenTop
        const newWindow = window.open(url, title, `
      scrollbars=yes,
      width=${w / systemZoom},
      height=${h / systemZoom},
      top=${top},
      left=${left}
      `)

        // if (window.focus) newWindow.focus();
        return newWindow;
    }

    function createElementFromHTML(htmlString) {
        var div = document.createElement('div');
        div.innerHTML = htmlString.trim();

        // Change this to div.childNodes to support multiple top-level nodes.
        return div.firstChild;
    }
})();