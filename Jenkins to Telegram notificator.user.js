// ==UserScript==
// @name         Jenkins to Telegram notificator
// @version      1.0.17
// @description  try to take over the world!
// @author       Nikilaienko Volodymyr
// @include      /^http:\/\/jenkins.*\/job\/.*\/[\d]+(\/|)$/
// @icon         https://www.google.com/s2/favicons?domain=temerix.com
// @grant        none
// @updateURL https://bitbucket.org/VolodymyrNik/plugins-and-helpers/raw/master/Jenkins%20to%20Telegram%20notificator.user.js
// ==/UserScript==
(function () {
    'use strict';

// Option()
    let id = "";
    const tokenForBot = "bot892720794:AAHXRSn0acK6zJKUkY8zWslsU2MoIWoTT1I";
//
    let myStorage = window.localStorage;
    id = id ? id : myStorage.getItem("telegramId");
    var btnIcon;
    var interval;
    let firststart = true;

    let parentElement = document.querySelector("#tasks");
    if (id) {
        parentElement.appendChild(createElementFromHTML(
            "<div class='task'>" +
            "<span class='task-link-wrapper'>" +
            "<a class='notification task-link' class='task-icon-link' >" +
            "<span class='task-icon-link'>" +
            "<img style='transform: scale(-1, 1);' src='/static/1dab921a/images/24x24/clock.gif' style='width: 24px; height: 24px; width: 24px; height: 24px; margin: 2px;' class='icon-next icon-md'>" +
            "</span>" +
            "<span class='task-link-text'>Notification</span></a></span></div>"));
    } else {
        parentElement.appendChild(createElementFromHTML(
            "<div class='task'>" +
            "<span class='task-link-wrapper'>" +
            "<a class='notification task-link' class='task-icon-link'>" +
            "<span class='task-icon-link'>" +
            "<img style='transform: scale(-1, 1);' src='/static/1dab921a/images/24x24/clock.gif' style='width: 24px; height: 24px; width: 24px; height: 24px; margin: 2px;' class='icon-next icon-md'>" +
            "</span>" +
            "<span class='task-link-text'>Setup</span></a></span></div>"));
    }

    let notificationBtn = document.querySelector(".notification")

    notificationBtn.addEventListener("click", function () {
        if (id)
            body();
        else
            setup();
    });

    btnIcon = notificationBtn.children[0];

    let isAlwaysOn = myStorage && myStorage.getItem("alarm") == "on";

    if (id && isAlwaysOn) {
        alwaysAlarm(true);
        body();
    }

    firststart = false;

    function checkJob() {
        let progressBar = document.querySelector(".build-caption-progress-container");
        console.log("check");
        if (progressBar != null && progressBar.style["display"] == "none" || !!$$("#build-flow-grid > .build-info.BLUE").size()) {
            var jobName = document.querySelector("#main-panel > h1").innerText;
            var msg = jobName + "\n\rEnd!\n\r" + document.URL + "allure/";
            fetch("https://api.telegram.org/" + tokenForBot + "/sendMessage?chat_id=" + id + "&text=" + encodeURI(msg).replace("#", ""));
            btnIcon.src = "/static/1dab921a/images/24x24/clock.png";
            clearInterval(interval);
        }
    }

    function body() {
        if (new String(btnIcon.src).include("clock") || (firststart || !isAlwaysOn)) {
            if (document.querySelectorAll(".build-caption span.icon-xlg")[0].className.include("anime")) {
                console.log("start");
                interval = setInterval(checkJob, 5000);
                btnIcon.src = "/static/1dab921a/images/24x24/clock_anime.gif";
                alwaysAlarm(true);
            } else {
                if (!isAlwaysOn) {
                    alwaysAlarm(true);
                } else {
                    //     alwaysAlarm(false);
                }
            }
        } else {
            console.log("stop");
            clearInterval(interval);
            btnIcon.src = "/static/1dab921a/images/24x24/clock.gif";
            alwaysAlarm(false);
        }
    }

    async function setup() {
        let url = "https://bitbucket.org/VolodymyrNik/plugins-and-helpers/raw/master/Notification/setup.html";
        const response = await fetch(url);
        let messages = await response.text();
        let newWin = popupCenter("", "", 800, 600);
        newWin.document.write(messages);
    }

    function alwaysAlarm(onoff) {
        if (onoff) {
            isAlwaysOn = true;
            myStorage.setItem("alarm", "on");
            $$("a.task-link.notification")[0].style['color'] = "limegreen";
            $$("a.task-link.notification img")[0].style = "transform: rotate(33deg);";
        } else {
            isAlwaysOn = false;
            myStorage.removeItem("alarm");
            $$("a.task-link.notification")[0].style['color'] = "black";
            $$("a.task-link.notification img")[0].style = "transform: scale(-1, 1);";
        }
    }

    function popupCenter(url, title, w, h) {
        // Fixes dual-screen position                             Most browsers      Firefox
        const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
        const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

        const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        const systemZoom = width / window.screen.availWidth;
        const left = (width - w) / 2 / systemZoom + dualScreenLeft
        const top = (height - h) / 2 / systemZoom + dualScreenTop
        const newWindow = window.open(url, title, `
      scrollbars=yes,
      width=${w / systemZoom},
      height=${h / systemZoom},
      top=${top},
      left=${left}
      `)
        return newWindow;
    }

    function createElementFromHTML(htmlString) {
        var div = document.createElement('div');
        div.innerHTML = htmlString.trim();

        // Change this to div.childNodes to support multiple top-level nodes.
        return div.firstChild;
    }
})();